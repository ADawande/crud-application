const AWS = require("aws-sdk");
const util = require("./dbUtils.js");
const LambdaInvoke = require('./LambdaInvoke');


exports.handler = async (event) => {
    console.log("This is base event: ", JSON.stringify(event));
   
let transactionId = event.transactionId ;
var fetchJSONFromDB = await util.getJSONByTransactionId(transactionId);
console.log(`Dynamo DB Document :${JSON.stringify(fetchJSONFromDB)}`);

if(fetchJSONFromDB === undefined || fetchJSONFromDB === null || fetchJSONFromDB === ""){
    console.log("Data From DB ============================ ",fetchJSONFromDB)
    return {
        error: true,
        errorMessage: "Unable To Fetch Data From DB"
    };
}else{
    let DOC =JSON.parse(fetchJSONFromDB.deliveryOrderContent);
    console.log(`Deliveryordercontent : ${DOC}`);
    
    let eventrecepientArray = event["preference"]["recipient"],
        dbrecepientArray = DOC["preference"]["recipient"]
    console.log(`event recepientarray type : ${eventrecepientArray.constructor === Array}`)    
    console.log(`DOC recepientarray type : ${dbrecepientArray.constructor=== Array}`) 
    console.log(`Event recepient array :${eventrecepientArray}`)
    console.log(`DynamoDb recepient array :${dbrecepientArray}`)

    for (let i in eventrecepientArray){
        let Eventrecepientid =  event["preference"]["recipient"][i]["recipientId"];
        console.log(`Event recipient id : ${Eventrecepientid}`);
       
            for(let j in dbrecepientArray){
                let DBrecepientid =  DOC["preference"]["recipient"][j]["recipientId"];
                console.log(`DB recipient id : ${DBrecepientid}`);
                let eventDeliveryMethod = event["preference"]["recipient"][i]["deliveryMethod"],
                    eventDeliveryChannel =  event["preference"]["recipient"][i]["deliveryChannel"],
                    eventDeliveryType =  event["preference"]["recipient"][i]["deliveryType"],
                    eventReceiveEmail =  event["preference"]["recipient"][i]["receiveEmail"]
                console.log("Event Receive Mail = ",eventReceiveEmail);
                if (Eventrecepientid === DBrecepientid){
                    console.log("updating the DB document");
                    if(eventDeliveryMethod.toLowerCase()==="email"){
                        DOC["preference"]["recipient"][j]["deliveryMethod"]= eventDeliveryMethod,  
                        DOC["preference"]["recipient"][j]["deliveryChannel"]=  eventDeliveryChannel,
                        DOC["preference"]["recipient"][j]["deliveryType"]= eventDeliveryType,
                        DOC["preference"]["recipient"][j]["receiveEmail"]= "true";
                        DOC["transactionId"] = event["transactionId"]
                        
                        let stringified_doc_email = JSON.stringify(DOC);
                        console.log(`Stringified DOC For Email :${stringified_doc_email}`);
                        
                        try {
                            let updateDBForEmailStatus = await util.updateDataToDb(transactionId, stringified_doc_email);
                            console.log(updateDBForEmailStatus);

                        } catch (error) {
                            return {
                                   error: true,
                                   errorMessage: "Database Updation Failed"
                             };
                        }

                       // Updating Resend Call In DB Attribute
                        try {
                            let updateDBForResendCall = await util.updateResendCall(transactionId, true);
                            console.log("Updating DB for Resend Call",updateDBForResendCall); 
                        } catch (error) {
                            return {
                                error: true,
                                errorMessage: "Resend Attribute Fails"
                          };
                        }

                        try {
                            console.log(`request being passed to start Comm :${DOC}`);

                           //calling the start comm lambda with the updated Delivery order content
                            var apicall = await LambdaInvoke.writeMessagetoSQS(DOC);
                            console.log("Start comm Response: ", apicall);   
                        } catch (error) {
                            return {
                                error: true,
                                errorMessage: "Api Call Fails"
                          };
                        }
                        
                        

                       
                        
                       
                        
                }
                    else if(eventDeliveryMethod.toLowerCase()==="sms"){
                        DOC["preference"]["recipient"][0]["deliveryMethod"]= eventDeliveryMethod,  
                        DOC["preference"]["recipient"][0]["deliveryChannel"]=  eventDeliveryChannel;
                        DOC["transactionId"] = event["transactionId"]

                        let stringified_doc_sms = JSON.stringify(DOC);
                        console.log(`Stringified DOC For Sms:${stringified_doc_sms}`);
                        //Updating the Deliveryordercontent 
                        let updateDBForSmsStatus = await util.updateDataToDb(transactionId, stringified_doc_sms);
                        console.log(updateDBForSmsStatus);
        
                        // Updating Resend Call In DB Attribute
                        let updateDBForResendCall = await util.updateResendCall(transactionId, true);
                        console.log("Updating DB for Resend Call",updateDBForResendCall);
                        //calling the start comm lambda with the updated Delivery order content
                        console.log(`request being passed to start Comm :${DOC}`)
                        var apicall = await LambdaInvoke.writeMessagetoSQS(DOC);
                        console.log("Start comm Response: ", apicall);
                       
                    }
                }
                else{
                    console.log("recepient id do not match");
                }
            }
    
       
    }

}




    









//lambda response
const response = {
    "transactionId": event.transactionId,
    "status": "SUCCESS"
};

return response;    
   
   
};


    
    
    
    
    
