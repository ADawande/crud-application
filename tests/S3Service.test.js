const AWS = require("./__mocks__/aws-sdk"); // eslint-disable-line  no-unused-vars
const {
    saveFile,
    getFile,
    deleteFile,
} = require("../src/services/S3Service");

describe("S3 Service Test", () => {
    let S3;

    beforeEach(() => {
        S3 = AWS.promise;
    });

    it("getFile - Success", async () => {
        S3.mockResolvedValue({ Body: Buffer.from("success") });
        const data = await getFile("bucket", "key");
        expect(data.toString('utf-8')).toEqual("success");
    });

    it("getFile - Failure", async () => {
        S3.mockRejectedValue("NoSuchKey");
        try {
            await getFile("bucket", "key");
        } catch (err) {
            expect(err).toEqual(new Error("NoSuchKey"));
        }
    });

    it("saveFile - Success", async () => {
        S3.mockResolvedValue("Filed Saved Successfully");
        const data = await saveFile("bucket", "key", "data", "contentType");
        expect(data).toEqual("Filed Saved Successfully");
    });

    it("saveFile - Failure", async () => {
        S3.mockRejectedValue("AccessDenied");
        try {
            await saveFile("bucket", "key", "data", "contentType");
        } catch (err) {
            expect(err).toEqual(new Error("AccessDenied"));
        }
    });

    it("deleteFile - success", async () => {
        S3.mockResolvedValue("Filed deleted Successfully");
        const data = await deleteFile("bucket", "key");
        expect(data).toEqual("Filed deleted Successfully");
    });

    it("deleteFile - failure", async () => {
        S3.mockRejectedValue("Error while deleting the object");
        try {
            await deleteFile("bucket", "key");
        } catch (err) {
            expect(err).toEqual(new Error("Error while deleting the object"));
        }
    });
});