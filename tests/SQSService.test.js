const AWS = require("./__mocks__/aws-sdk"); // eslint-disable-line  no-unused-vars
const {
    sendToQueue,
    writeMessagetoSQS,
} = require("../src/services/SQSService");
const mockRequest = require("./mock/request.json")

describe("SQS Service Test", () => {
    let SQS;

    beforeEach(() => {
        SQS = AWS.promise;
    });

    beforeAll(() => {
        process.env.EXTERNAL_STATUS_API_QUEUE = "ExternalStatusAPI"
    })

    it("sendSQSMessage - success", async () => {
        SQS.mockResolvedValue("Posted Message to SQS Successfully");
        const data = await sendToQueue("queueURL","12334");
        expect(data).toEqual("Posted Message to SQS Successfully");
    });

    it("sendSQSMessage - Failure", async () => {
        SQS.mockRejectedValue("NonExistentQueue");
        try{
        const data = await sendToQueue("nonExistentQueueUrl","12334");
        }catch(err){
            expect(err).toEqual(new Error("NonExistentQueue"));
        }
    });

    it("writeMessagetoSQS - success", async () => {
        SQS.mockResolvedValue("Posted Message to SQS Successfully");
        const data = await writeMessagetoSQS(mockRequest.sqsInputTransactionJson);
        expect(data).toEqual("Posted Message to SQS Successfully");
    });

    it("writeMessagetoSQS - Failure", async () => {
        SQS.mockRejectedValue("NonExistentQueue");
        try{
        const data = await writeMessagetoSQS(mockRequest.sqsInputTransactionJson);
        }catch(err){
            expect(err).toEqual("NonExistentQueue");
        }
    });

});