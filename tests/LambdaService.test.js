const AWS = require("./__mocks__/aws-sdk"); // eslint-disable-line  no-unused-vars
const invokeLambdaSvc = require("../src/services/LambdaService");

describe("lambda Invoke", () => {

    it("invoke lambda - success", async () => {
        const result = await invokeLambdaSvc.invoke("lambdaToInvoke", "payload", invocationType = "RequestResponse", "0d7132dc-2b70-4456-b5e8-3026f07cd230", "HXT");
        expect(result).not.toBeNull();
    });

    it("invoke lambda - failure", async () => {
        try {
            const result = await invokeLambdaSvc.invoke("lambdaToInvoke", "payload", invocationType = "RequestResponse", "0d7132dc-2b70-4456-b5e8-3026f07cd230", "HXT");
        } catch (err) {
            expect(err.message).toEqual("ERROR");
        }
    });

});