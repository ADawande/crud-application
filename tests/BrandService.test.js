const AWS = require("./__mocks__/aws-sdk"); // eslint-disable-line  no-unused-vars
const {
	getBrandById,
	getBrandBySourceSystem,
} = require("../src/services/BrandService");
const mockRequest = require("./mock/request.json");
const mockResponse = require("./mock/response.json");
const { fetchBrandInfo } = require("@delmgr/apputils/businessUtil/brandUtils.js");

jest.mock("@delmgr/apputils/businessUtil/brandUtils.js");
fetchBrandInfo.mockResolvedValue(mockResponse.brandResponse);

describe("testing brandService", () => {
	afterAll(() => {
		jest.resetAllMocks();
	});

	beforeAll(() => {
		process.env.IPM_BRAND_TABLE = "br_ipm_dev_ipm_brand_table-0"
	});
	let dynamoDB;
	beforeEach(() => {
		dynamoDB = new AWS.DynamoDB.DocumentClient()
	})


	it("should return the brand item with getBrandById", async () => {
		const result = await getBrandById(
			mockRequest.getBrandRequest.getBrandByBrandId.brandId
		);
		expect(result).toBeDefined();
		expect(result).not.toBeNull();
		expect(result).toEqual(mockResponse.brandResponse);
	});

	it("should return the brand item with getBrandBySourceSystem", async () => {
		fetchBrandInfo.mockResolvedValue(mockResponse.dynamoDbScanResponse.Items);
		const result = await getBrandBySourceSystem(
			mockRequest.getBrandRequest.getBrandBySourceSystem.sourceSystemId,
			mockRequest.getBrandRequest.getBrandBySourceSystem.sourceSystemBrandId,
			mockRequest.getBrandRequest.getBrandBySourceSystem.onPremBrandId
		);
		expect(result).toBeDefined();
		expect(result).not.toBeNull();
		expect(result).toEqual(mockResponse.dynamoDbScanResponse.Items[0]);
	});

	it("should not return the brand item with getBrandById", async () => {
		fetchBrandInfo.mockRejectedValue({ message: "ERROR" });
		try{
		const result = await getBrandById(
			mockRequest.getBrandRequest.getBrandByBrandId.brandId
		);
		}catch(err){
			expect(err.message).toEqual("Error getting item ERROR");
		}
	});

	it("should not return the brand item with getBrandBySourceSystem", async () => {
		fetchBrandInfo.mockResolvedValue(mockResponse.dynamoDbScanResponseNull.Items);
		expect.assertions(1);
		try {
			const result = await getBrandBySourceSystem(
				mockRequest.getBrandRequest.getBrandBySourceSystem.sourceSystemId,
				mockRequest.getBrandRequest.getBrandBySourceSystem.sourceSystemBrandId,
				mockRequest.getBrandRequest.getBrandBySourceSystem.onPremBrandId
			);
			expect(result).toBeNull();
		} catch (e) {
			expect(e.message).toEqual("Error: BRAND_NULL_OR_NOT_UNIQUE");
		}
	});

});