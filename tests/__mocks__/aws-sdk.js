// mockable, resolvable promise used inside test cases
const promise = jest.fn().mockReturnValue(Promise.resolve(true));

// mock response from aws service call
const response = jest.fn().mockImplementation(() => ({ promise }));

class DocumentClient {
    get = response;
    put = response;
    update = response;
    scan = response;
  }
  
  // Mock DynamoDB
  const DynamoDB = {
    DocumentClient,
  };
  
  // Mock SQS
  class SQS {
    sendMessage = response;
  }
  
  // Mock Lambda
  class Lambda {
    invoke = response;
  }
  
  // Mock S3
  class S3 {
    putObject = response;
    getObject = response;
    deleteObject = response;
  }

  class STS {
    assumeRole = response;
  }

  class RDS {
    executeStatement = response;
  }

  const mockedConfig = {
    update: jest.fn()
  };
  
  module.exports = {
    SQS,
    promise,
    Lambda,
    S3,
    STS,
    RDS,
    DynamoDB,
    config: mockedConfig
  };