const AWS = require("./__mocks__/aws-sdk"); // eslint-disable-line  no-unused-vars
const mockRequest = require("./mock/request.json");
const mockResponse = require("./mock/response.json");
const { handler } = require("../src/index");

describe("Calling handler to test", () => {

	afterAll(() => {
		jest.resetAllMocks();
	});

	let dynamoDB;
	let S3;
	let SQS;
	let lambda;

	beforeEach(() => {
		dynamoDB = new AWS.DynamoDB.DocumentClient();
		S3 = AWS.promise;
		SQS = AWS.promise;
		lambda = AWS.promise;
		config = AWS.promise;
	})


	beforeAll(() => {
		process.env.BRAND_TABLE = "br_ics_dev_delmgr_brand_table-0"
		process.env.DEP_DELIVERY_TABLE = "br_ics_dev_delmgr_email_delivery_table-0"
		process.env.DEP_DM_BUFFER_BUCKET_NAME = "br-icsdev-delmgr-dep-response-data-us-east-1-s3"
		process.env.DEP_EMAIL_CLICK_TABLE = "br_ics_dev_delmgr_email_click_table-0"
		process.env.DEP_EMAIL_DELIVERY_TABLE = "br_ics_dev_delmgr_email_delivery_table-0"
		process.env.DEP_EMAIL_EFAIL_TABLE = "br_ics_dev_delmgr_email_efail_table-0"
		process.env.DEP_EMAIL_FBL_TABLE = "br_ics_dev_delmgr_email_fbl_table-0"
		process.env.DEP_EMAIL_OPEN_TABLE = "br_ics_dev_delmgr_email_open_table-0"
		process.env.DEP_EMAIL_RETRY_TABLE = "br_ics_dev_delmgr_email_retry_table-0"
		process.env.DIS_INTEGRATION_LAMBDA = "br_icsdev_delmgr_lambda_account_preference_flip_service"
		process.env.DM_ACCOUNT_PREF_FLIP_LAMBDA = "br_icsdev_delmgr_lambda_account_preference_flip_service"
		process.env.DOCUMENT_DELIVERY = "br_ics_dev_delmgr_document_delivery_table-0"
		process.env.DOCUMENT_DELIVERY_BATCH = "br_ics_dev_delmgr_document_delivery_batch_table-0"
		process.env.ELASTIC_PORT = "80"
		process.env.ELASTIC_SCHEME = "http"
		process.env.EMAIL_OPT_OUT_QUEUE = "DEV_DM_Optout_Queue"
		process.env.ENV = "dev"
		process.env.ES_INDEX_NAME = "enrollment"
		process.env.ES_URL = 'https://vpc-dev-delmgr-rpt-es-wjerze3t7ur257feysg5vm3mfa.us-east-1.es.amazonaws.com'
		process.env.NETWORKADDRESS_CACHE_TTL = "60"
		process.env.ON_PREM_TO_CLOUD_SYNC_BUCKET = "dev-dm-on-prem-data-sync"
		process.env.UEM_ANALYTICS_BUCKET_NAME = "uem-analytics"
		process.env.BRAND_EFAIL_PREFIX_LASTATTEMPT_REASON = "HXT"
	})


	it("to check if the event is defined", async () => {
		expect(mockRequest.handlerEvent).toBeDefined();
	});

	it("to check if the event is not null", async () => {
		expect(mockRequest.handlerEvent).not.toBeNull();
	});
	it("to check if the record is defined", async () => {
		expect(mockRequest.handlerEvent.Records).toBeDefined();
	});
	it("to check if the record is not null", async () => {
		expect(mockRequest.handlerEvent.Records).not.toBeNull();
	});

	it("to check if the record has data", async () => {
		expect(mockRequest.handlerEvent.Records.length).not.toEqual(0);
	});
	
});
