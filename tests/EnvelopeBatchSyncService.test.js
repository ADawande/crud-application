const AWS = require("./__mocks__/aws-sdk"); // eslint-disable-line  no-unused-vars

const mockRequest = require("./mock/request.json");
const mockResponse = require("./mock/response.json");
const {
    getDocument,
    updateDocumentStatus,
    updateDocument,
    updateBatchCounts,
    updateBatchStatus,
    getActivityAttributes,
    addEmailTrackingEvent
} = require("../src/services/EnvelopeBatchSyncService")

const {
    timestampNow,
    timestampAsUTC,
    getEmailSubject,
    timestampMSAsUTC,
    isEmpty
} = require("../src/utils/EnvelopeSyncUtils");

jest.mock("../src/utils/EnvelopeSyncUtils");
timestampNow.mockResolvedValue("2023-10-18T12:06:34.488Z");
timestampAsUTC.mockResolvedValue("2023-10-04T11:16:23.000Z");
timestampMSAsUTC.mockResolvedValue("2023-10-04T11:16:23.000Z");
isEmpty.mockResolvedValue(false);
getEmailSubject.mockResolvedValue("This is test for RTE - Fully Composed")


describe("Envelope Batch Sync Service Test", () => {
    let dynamoDbClient;
    let SQS;

    beforeEach(() => {
		dynamoDbClient = AWS.promise;
        SQS = AWS.promise;
	})

    beforeAll(() => {
        process.env.DOCUMENT_DELIVERY = "br_ics_dev_delmgr_document_delivery_table-0",
        process.env.BRAND_EFAIL_PREFIX_LASTATTEMPT_REASON = "HXT",
        process.env.DEP_EMAIL_EFAIL_TABLE = "br_ics_dev_delmgr_email_efail_table-0",
        process.env.DEP_RESPONSE_SYNC_SQS_URL = "dep_resync_sqs",
        process.env.DOCUMENT_DELIVERY_BATCH = "br_ics_dev_delmgr_document_delivery_batch_table-0"
    })

    it("getDocument - success", async () => {
        dynamoDbClient.mockResolvedValue(mockResponse.dynamoDbGetResponse);
        const data = await getDocument("0d7132dc-2b70-4456-b5e8-3026f07cd230");
        expect(data).toEqual(mockResponse.dynamoDbGetResponse.Item);
    });

    it("getDocument - failure", async () => {
        dynamoDbClient.mockRejectedValue("InternalServerError");
        try{
        const data = await getDocument("0d7132dc-2b70-4456-b5e8-3026f07cd230");
        }catch(err){
            expect(err).toEqual(new Error("InternalServerError"));
        }
    });

    it("updateDocument - success", async () => {
        dynamoDbClient.mockResolvedValue(mockResponse.dynamoDbUpdateResponse);
        const data = await updateDocument("0d7132dc-2b70-4456-b5e8-3026f07cd230", mockResponse.dynamoDbUpdateResponse.Item.deliveryOrderContent, "HXT");
        expect(data).toEqual(mockResponse.dynamoDbUpdateResponse.Item);
    });

    it("updateDocument - failure", async () => {
        dynamoDbClient.mockRejectedValue("InternalServerError");
        try{
        const data = await updateDocument("0d7132dc-2b70-4456-b5e8-3026f07cd230", mockResponse.dynamoDbUpdateResponse.Item.deliveryOrderContent, "HXT");
        }catch(err){
            expect(err);
        }
    });

    it("getActivityAttributes - Delivery - success", async () => {
        timestampNow.mockResolvedValue("2023-10-18T12:06:34.488Z");
        timestampAsUTC.mockResolvedValue("2023-10-18T11:16:23.000Z");
        timestampMSAsUTC.mockResolvedValue("2023-10-18T11:16:23.000Z");
        const data = await getActivityAttributes("0d7132dc-2b70-4456-b5e8-3026f07cd230",mockResponse.depEventDelivery,mockResponse.dynamoDbGetResponse.Item,"HXT");
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
    });

    it("getActivityAttributes - Bounce - success", async () => {
        timestampNow.mockResolvedValue("2023-10-18T12:06:34.488Z");
        timestampAsUTC.mockResolvedValue("2023-10-18T11:16:23.000Z");
        timestampMSAsUTC.mockResolvedValue("2023-10-18T11:16:23.000Z");
        const data = await getActivityAttributes("0d7132dc-2b70-4456-b5e8-3026f07cd230",mockResponse.depEventBounce,mockResponse.dynamoDbGetResponse.Item,"HXT");
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
    });

    it("getActivityAttributes - Failure", async () => {
        try{
        const data = await getActivityAttributes("0d7132dc-2b70-4456-b5e8-3026f07cd230",mockResponse.depEventBounce,mockResponse.dynamoDbGetResponse.Item,"HXT");
        }catch(err){
            expect(err);
        }
    });

    it("addEmailTrackingEvent - Success", async () => {
        SQS.mockResolvedValue("Posted Message to SQS Successfully");
        const data = await addEmailTrackingEvent("EmailDelivery",mockResponse.depEventDelivery,mockResponse.dynamoDbGetResponse.Item.deliveryOrderContent.additionalAttributes.secondaryEmailInfo,"HXT", "delivery");
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
        expect(data).toBeTruthy();
    });

    it("addEmailTrackingEvent - Failure", async () => {
        SQS.mockRejectedValue("NonExistentQueue");
        try{
        const data = await addEmailTrackingEvent("EmailDelivery",mockResponse.depEventDelivery,mockResponse.dynamoDbGetResponse.Item.deliveryOrderContent.additionalAttributes.secondaryEmailInfo,"HXT", "delivery");
        }catch(err){
            expect(err);
        }
    });

    it("updateBatchStatus - Success", async () => {
        dynamoDbClient.mockResolvedValue(mockResponse.batchTableResponse);
        const data = await updateBatchStatus(mockResponse.batchTableResponse, "0d7132dc-2b70-4456-b5e8-3026f07cd230", "HXT");
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
    });

    it("updateBatchStatus - Failure", async () => {
        dynamoDbClient.mockRejectedValue("InternalServerError");
        try{
        const data = await updateBatchStatus(mockResponse.batchTableResponse, "0d7132dc-2b70-4456-b5e8-3026f07cd230", "HXT");
        }catch(err){
            expect(err)
        }
    });



});