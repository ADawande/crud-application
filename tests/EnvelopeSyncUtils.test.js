const Constants = require("../src/constants");
const mockRequest = require("./mock/request.json");
const mockResponse = require("./mock/response.json");
const {getBrandById} = require("../src/services/BrandService");

jest.mock("../src/services/BrandService");
getBrandById.mockResolvedValue(mockResponse.brandResponse);

const {
    timestampAsUTC,
	timestampMSAsUTC,
	timestampNow,
	isCCorBCC,
	hasBatchId,
	hasFileName,
	getTemplateName,
	requireStatusUpdate,
	parseDMData,
	getEmailTrackingTable,
	isEmpty,
	getEmailSubject,
	isClickLinkUnsubscribe,
	isEventSourceSQS,
	isBrandConfiguredForOptOutEmail,
	isBrandOptedForHandShakeEvent,
	getTemplateNameUEM,
	getTemplateNameEM,
	getPreferenceChangeOnBounceConfig
} = require("../src/utils/EnvelopeSyncUtils");

describe("Envelope Sync Utils Test", () => {

    beforeAll(() => {
        process.env.DEP_DELIVERY_TABLE = "br_ics_dev_delmgr_email_delivery_table-0"
 	    process.env.DEP_DM_BUFFER_BUCKET_NAME = "br-icsdev-delmgr-dep-response-data-us-east-1-s3"
 	    process.env.DEP_EMAIL_CLICK_TABLE = "br_ics_dev_delmgr_email_click_table-0"
 	    process.env.DEP_EMAIL_DELIVERY_TABLE = "br_ics_dev_delmgr_email_delivery_table-0"
 	    process.env.DEP_EMAIL_EFAIL_TABLE = "br_ics_dev_delmgr_email_efail_table-0"
 	    process.env.DEP_EMAIL_FBL_TABLE = "br_ics_dev_delmgr_email_fbl_table-0"
 	    process.env.DEP_EMAIL_OPEN_TABLE = "br_ics_dev_delmgr_email_open_table-0"
 	    process.env.DEP_EMAIL_RETRY_TABLE = "br_ics_dev_delmgr_email_retry_table-0"
    })
    
    it("timestampAsUTC", async () => {
        const data = timestampAsUTC("1696418183");
        expect(data).toEqual("2023-10-04T11:16:23.000Z");
    });

    it("timestampMSAsUTC", async () => {
        const data = timestampMSAsUTC("1696418183017");
        expect(data).toEqual("2023-10-04T11:16:23.017Z");
    });

    it("timestampNow", async () => {
        const data = timestampNow();
        expect(data).not.toBeNull();
    });

    it("isCCorBCC - Success", async () => {
        let data = isCCorBCC(mockResponse.rcptMetaCC);
        expect(data).toBeTruthy();
    });

    it("isCCorBCC - Failure", async () => {
        let data = isCCorBCC(mockResponse.rcptMetaTo);
        expect(data).toBeFalsy();
    });

    it("hasBatchId - Success", async () => {
        let data = hasBatchId(mockResponse.batchId);
        expect(data).toBeTruthy();
    });

    it("hasBatchId - Failure", async () => {
        let data = hasBatchId(mockResponse.batchIdNA);
        expect(data).toBeFalsy();
    });

    it("hasFileName - Success", async () => {
        let data = hasFileName(mockResponse.dynamoDbGetResponse.Item.fileName);
        expect(data).toBeTruthy();
    });

    it("hasFileName - Failure", async () => {
        let data = hasFileName(mockResponse.fileNameNA);
        expect(data).toBeFalsy();
    });

    it("isEventSourceSQS - Success", async () => {
        let data = isEventSourceSQS(mockRequest.handlerEvent);
        expect(data).toBeTruthy();
    });

    it("isEventSourceSQS - Failure", async () => {
        let data = isEventSourceSQS(mockRequest.NullEvent);
        expect(data).toBeFalsy();
    });


    it("getTemplateName - Success", async () => {
        let data = getTemplateName(mockResponse.dynamoDbGetResponse.Item);
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
    });

    it("getTemplateName - Failure", async () => {
        let data = getTemplateName(mockResponse.dynamoDbGetResponseWithNullTemplateName.Item);
        expect(data).toBe("");
    });

    it("requireStatusUpdate - true", async () => {
        let data = requireStatusUpdate(mockResponse.dynamoDbGetResponse.Item, mockResponse.depEventDelivery);
        expect(data).toBeTruthy();
    });

    it("requireStatusUpdate - true", async () => {
        let data = requireStatusUpdate(mockResponse.dynamoDbGetResponseWithNullTemplateName.Item, mockResponse.depEventDelivery);
        expect(data).toBeTruthy();
    });

    it("requireStatusUpdate - false", async () => {
        let data = requireStatusUpdate(mockResponse.dynamoDbGetResponse.Item, mockResponse.depEventBounce);
        expect(data).toBeFalsy();
    });

    it("requireStatusUpdate - false", async () => {
        let data = requireStatusUpdate(mockResponse.dynamoDbGetResponse.Item, mockResponse.depEventBounceWithoutMS);
        expect(data).toBeFalsy();
    });

    it("getEmailSubject - Fullycomposed", async () => {
        let data = getEmailSubject(mockResponse.dynamoDbGetResponse.Item);
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
        expect(data).toEqual("This is test for RTE - Fully Composed");
    });

    it("getEmailSubject - From EM Response", async () => {
        let data = getEmailSubject(mockResponse.txnItemWithEMResponse);
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
        expect(data).toEqual("This is test for RTE")
    });

    it("getEmailSubject - From UEM Response", async () => {
        let data = getEmailSubject(mockResponse.txnItemWithUEMResponse);
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
        expect(data).toEqual("Test email ICP");
    });

    it("getEmailSubject - From Transaction Item", async () => {
        let data = getEmailSubject(mockResponse.txnItemWithEmailSub);
        expect(data).toBeDefined();
        expect(data).not.toBeNull();
        expect(data).toEqual("RTE Email");
    });

    it("getTemplateNameUEM - From UEM Response - Success", async () => {
        let data = await getTemplateNameUEM(mockResponse.txnItemWithUEMResponse);
        expect(data).toEqual("CFA Email")
    });

    it("getTemplateNameUEM - From UEM Response - Failure", async () => {
        let data = await getTemplateNameUEM(mockResponse.txnItemWithUEMResponseNoTempName);
        expect(data).toEqual("")
    });

    it("getTemplateNameEM - From EM Response - Success", async () => {
        let data = await getTemplateNameEM(mockResponse.txnItemWithEMResponse);
        expect(data).toEqual("CFA Email")
    });

    it("getTemplateNameEM - From EM Response - Failure", async () => {
        let data = await getTemplateNameEM(mockResponse.txnItemWithEMResponseNoTempName);
        expect(data).toEqual("")
    });

    it("getPreferenceChangeOnBounceConfig - Success", async () => {
        let data = getPreferenceChangeOnBounceConfig(mockResponse.dynamoDbGetResponse.Item);
        expect(data).toBeDefined()
        expect(data).not.toBeNull()
        expect(data).toEqual(mockResponse.preferenceChangeOnBounceConfig)
    });

    it("parseDMData - RTE - Success", async () => {
        let data = parseDMData(mockResponse.dynamoDbGetResponse.Item, true);
        expect(data).toBeDefined()
        expect(data).not.toBeNull()
    })

    it("parseDMData - Batch - Success", async () => {
        let data = parseDMData(mockResponse.dynamoDbGetResponse.Item, false);
        expect(data).toBeDefined()
        expect(data).not.toBeNull()
    })

    it("isBrandConfiguredForOptOutEmail - Success", async () => {
        let data = isBrandConfiguredForOptOutEmail(mockResponse.dynamoDbGetResponse.Item);
        expect(data).toBeTruthy()
    })

    it("isBrandOptedForHandShakeEvent - Success", async () => {
        getBrandById.mockResolvedValue(mockResponse.brandResponse);
        let data = await isBrandOptedForHandShakeEvent(mockResponse.dynamoDbGetResponse.Item);
        expect(data).toBeTruthy() 
    })

    it("isBrandOptedForHandShakeEvent - failure", async () => {
        getBrandById.mockRejectedValue("InternalServerError");
        try{
            let data = await isBrandOptedForHandShakeEvent(mockResponse.dynamoDbGetResponse.Item);
        }catch(err){
            expect(err).toEqual(new Error("InternalServerError"));
        }
    });

    it("getEmailTrackingTable - delivery", async () => {
        let data = getEmailTrackingTable("delivery");
        expect(data).toEqual("br_ics_dev_delmgr_email_delivery_table-0")
    })

    it("getEmailTrackingTable - bounce", async () => {
        let data = getEmailTrackingTable("bounce");
        expect(data).toEqual("br_ics_dev_delmgr_email_efail_table-0")
    })

    it("getEmailTrackingTable - open", async () => {
        let data = getEmailTrackingTable("open");
        expect(data).toEqual("br_ics_dev_delmgr_email_open_table-0")
    })

});



