const mongoose = require('mongoose')

const employeSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    tech: {
        type: String,
        required: true
    },
    position: {
        type: String,
        required: true
    }

})

module.exports = mongoose.model('EmployeSchema', employeSchema)