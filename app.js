const express = require('express')
const mongoose = require('mongoose')
const path = require('path')
const web = require("./routes/web")
const ejs = require("ejs")

const port = '3000'
const app = express()

const DATABASE_URL = 'mongodb://localhost/EmployeeData'
mongoose.connect(DATABASE_URL, { useNewURLParser: true });
const con = mongoose.connection
con.on('open', () => {
    console.log('connected...')
})

//MiddleWare
app.use(express.urlencoded({ extended: false }))

app.use('/employe', express.static(path.join(process.cwd(), "public")));
app.use('/employe/edit', express.static(path.join(process.cwd(), "public")));

//Load Routes
app.use("/employe", web)

// Set Template Engine
app.set("view engine", 'ejs')

app.listen(port, () =>
    console.log(`Server at http://localhost:${port}/employe`)
)