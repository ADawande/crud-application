
const AWS = require("aws-sdk");
const dynamoDbClient = new AWS.DynamoDB.DocumentClient();


// Fetching Json From DB

async function getJSONByTransactionId(transactionId){
    console.log("Inside Get Json By Transaction Id");
    try {
        console.log("Getting transactionId:\n", transactionId);
        var params = {
            ConsistentRead: true,
            TableName: process.env.DOCUMENT_DELIVERY_TABLE,
            Key: {
                    "transactionId": transactionId
            }
        };
        console.log("Param=========================================",params)
        var result = await dynamoDbClient.get(params).promise();
        console.log("Res:",result);
        return result;
    } catch (error) {
        console.log("Error === ",error);
    }
}

// Updating Resend Call In Dynamodb

async function updateResendCall(transactionId, resendCallValue) {
   
    return new Promise(async(resolve, reject) => {
    
      try {

        console.log("Updating Main Status for resend :\n", transactionId, resendCallValue);

        var params = {
            ConsistentRead: true,
            TableName: process.env.DOCUMENT_DELIVERY_TABLE,
            Key: {
                'transactionId': transactionId
            },
            UpdateExpression: "set #bf.#attri.#rescall = :x",
            ExpressionAttributeNames: {
                "#bf": "brandProfile",
                "#attri": "additionalAttributes",
                "#rescall": "resendCall"
            },
            ExpressionAttributeValues : {
                 ":x": resendCallValue
            },
            ReturnValues: "UPDATED_NEW"
        };
        let data = await dynamoDbClient.update(params).promise();
        resolve(data);
      }catch (err) {
        console.log("DynamoDB Error:\n", err);
        reject(false);
    }
        

    });
}



async function updateDataToDb(transactionId, doc) {

    return new Promise(async(resolve,reject) => {
      try {

        console.log("Updating New Data To DB:\n", transactionId, doc);
        var params = {
            ConsistentRead: true,
            TableName: process.env.DOCUMENT_DELIVERY_TABLE,
            Key: {
                'transactionId': transactionId
            },
            UpdateExpression: "set #newdoc = :x",
            ExpressionAttributeNames: {
                 "#newdoc": "deliveryOrderContent",
               
            },
            ExpressionAttributeValues : {
                 ":x": doc
            },
            ReturnValues: "UPDATED_NEW"
           };
        let data = await dynamoDbClient.update(params).promise();
        resolve(data);
      }catch (err) {
        console.log("DynamoDB Error:\n", err);
        reject(false);
    }
        
});
    
}

module.exports = {
    getJSONByTransactionId: getJSONByTransactionId,
    updateResendCall:updateResendCall,
    updateDataToDb : updateDataToDb
}

