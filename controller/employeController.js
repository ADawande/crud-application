const { render } = require("ejs")
const EmployeSchema = require('../db/connectdb.js')

class employeController {

    static createDoc = async(req, res) => {
        try {
            const { name, tech, position } = req.body
            const doc = new EmployeSchema({
                    name: name,
                    tech: tech,
                    position: position
                })
                // Saving Document
            const result = await doc.save()
            res.redirect("/employe")
        } catch (error) {
            console.log(error);
        }
    }

    static getAllDoc = async(req, res) => {
        try {
            const result = await EmployeSchema.find()
            res.render("index", { data: result })
        } catch (error) {
            console.log(error)
        }

    }

    static editDoc = async(req, res) => {
        try {
            const result = await EmployeSchema.findById(req.params.id)
            res.render("edit", { data: result })
        } catch (error) {
            console.log(Erroe);
        }

    }

    static updateDocById = async(req, res) => {
        // console.log(req.params.id);
        // console.log(req.body);
        try {
            const result = await EmployeSchema.findByIdAndUpdate(req.params.id, req.body)
                // console.log(result);
        } catch (error) {
            console.log(error);
        }
        res.redirect("/employe")
    }

    static deleteDocById = async(req, res) => {
        console.log(req.params.id);
        try {
            const result = await EmployeSchema.findByIdAndDelete(req.params.id)
            res.redirect("/employe")
        } catch (error) {
            console.log(Error);
        }

    }
}

module.exports = employeController