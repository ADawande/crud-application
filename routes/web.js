const express = require('express')
const router = express.Router();

const employeController = require("../controller/employeController")

router.get('/', employeController.getAllDoc)

router.post('/', employeController.createDoc)

router.get('/edit/:id', employeController.editDoc)

router.post('/update/:id', employeController.updateDocById)

router.post('/delete/:id', employeController.deleteDocById)

module.exports = router